<?php
require_once __DIR__.'/vendor/autoload.php';

use Silex\Application;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;

$app = new Application();

$app['debug'] = true;

$app->register(new TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/src/FeedReader/Views',
]);

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/logs/feedreader.log',
));

$app->register(new ServiceControllerServiceProvider());
