Feature: Realtime processing large XML feed
  Visiting the home page, fill in product XML feed URI and submit
  should read products from the feed in memory efficient way
  and render them in user friendly layout.

  Scenario: Submit an invalid URL
    Given I am on "/products.json?feedURI=abcde"
    Then the response status code should be 200
    Then the response should contain "Product XML feed [abcde] is not a valid URL"

  Scenario: Submit a URI in valid format but cannot be opened.
    Given I am on "/products.json?feedURI=http%3A%2F%2F172.17.0.1%2Fassets%2Fdata%2Fproductfeed.xml"
    Then the response status code should be 200
    Then the response should contain "Failed to open product XML feed"

  Scenario: Submit valid URI.
    Given I am on "/products.json?feedURI=http%3A%2F%2Fpf.tradetracker.net%2F%3Faid%3D1%26type%3Dxml%26encoding%3Dutf-8%26fid%3D251713%26categoryType%3D2%26additionalType%3D2%26limit%3D10"
    Then the response status code should be 200
    Then the response should contain "productID"

