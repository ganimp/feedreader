<?php
namespace FeedReader\Services;

use DOMDocument;
use FeedReader\Exceptions\InvalidProductException;
use FeedReader\Exceptions\OpenFeedURIFailedException;
use FeedReader\Models\Product;
use FeedReader\Models\ProductCollection;
use Silex\Application;
use XMLReader;

class ProductReader
{
    /**
     * Silex Application
     *
     * @var Application
     */
    private $app;

    /**
     * Number of products to stream
     *
     * @var int
     */
    private $numOfProductsToStream;

    /**
     * XML pull parser
     *
     * @var XMLReader
     */
    private $reader;

    /**
     * Boolean flag indicates whether feedURI opened or not
     *
     * @var bool
     */
    private $openedFeedURI = false;

    /**
     * Represents an XML document
     *
     * @var DomDocument
     */
    private $dom;

    /**
     * Boolean flag indicates end of feed has reached or not
     *
     * @var bool
     */
    private $eof = false;

    /**
     * ProductReader constructor.
     *
     * @param Application $app                   - silex application
     * @param XMLReader   $reader                - XML pull parser
     * @param DOMDocument $dom                   - represents an XML document
     * @param int         $numOfProductsToStream - number of products to stream
     */
    public function __construct(
        Application $app,
        XMLReader $reader,
        DOMDocument $dom,
        int $numOfProductsToStream = 5
    ) {
        $this->app = $app;
        $this->reader = $reader;
        $this->dom = $dom;
        $this->numOfProductsToStream = $numOfProductsToStream;
    }

    /**
     * Reads feedURI and returns products collection
     *
     * @param string $feedURI - product feed URI
     *
     * @return ProductCollection
     */
    public function stream(string $feedURI) : ProductCollection
    {
        $this->open($feedURI);
        $productCollection = new ProductCollection();
        $counter = 0;
        while ($this->reader->read()) {
            if ($this->getNameOfCurrentNode() === 'product') {
                $node = $this->dom->importNode($this->reader->expand(), true);
                $element = simplexml_import_dom($node);
                try {
                    $productCollection->add(Product::create($element));
                    $counter++;
                } catch (InvalidProductException $exp) {
                    $this->app['monolog']->warning(
                        sprintf(
                            "Problem occurred while reading feed [%s]. Error message: [%s], Product XML element: [%s].",
                            $feedURI,
                            $exp->getMessage(),
                            $exp->getProductXML()
                        )
                    );
                }
                $this->reader->next();
                if ($counter >= $this->numOfProductsToStream) {
                    return $productCollection;
                }
            }
        }
        $this->close();
        return $productCollection;
    }

    /**
     * Reached end of feed
     *
     * @return bool
     */
    public function eof() : bool
    {
        return $this->eof === true;
    }

    /**
     * Opens the feed URI if it's not opened before
     *
     * @param string $feedURI - product feed URI
     *
     * @return void
     */
    protected function open(string $feedURI)
    {
        if (!$this->openedFeedURI) {
            if (!@$this->reader->open($feedURI)) {
                throw new OpenFeedURIFailedException(
                    sprintf(
                        'Failed to open the feed URI [%s]',
                        $feedURI
                    )
                );
            }
            $this->openedFeedURI = true;
        }
    }

    /**
     * Marks reaching end of feed and close the reader
     *
     * @return void
     */
    protected function close()
    {
        $this->eof = true;
        $this->reader->close();
    }

    /**
     * Returns the name of current node
     *
     * @return string
     */
    protected function getNameOfCurrentNode() : string
    {
        return $this->reader->name;
    }
}
