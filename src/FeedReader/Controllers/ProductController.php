<?php
namespace FeedReader\Controllers;

use FeedReader\Exceptions\OpenFeedURIFailedException;
use Silex\Application;
use FeedReader\Models\EventStream;
use FeedReader\Services\ProductReader;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductController
{
    /**
     * Silex Application
     *
     * @var Application
     */
    private $app;

    /**
     * Request stack that controls the lifecycle of requests
     *
     * @var RequestStack
     */
    private $requestStack;

    /**
     * Service that reads products from feedURI
     *
     * @var ProductReader
     */
    private $productReader;

    /**
     * Represents a streamed HTTP response
     *
     * @var StreamedResponse
     */
    private $streamedResponse;

    /**
     * ProductController constructor.
     *
     * @param Application      $app              - silex application
     * @param StreamedResponse $streamedResponse - streamed HTTP response
     */
    public function __construct(
        Application $app,
        StreamedResponse $streamedResponse
    ) {
        $this->app = $app;
        $this->requestStack = $app['request_stack'];
        $this->productReader = $app['product.reader'];
        $this->streamedResponse = $streamedResponse;
    }

    /**
     * Reads feedURI and streams product details
     *
     * @return StreamedResponse
     */
    public function getProductsJsonAction()
    {
        $feedURI = $this->requestStack->getCurrentRequest()->get('feedURI');
        $this->app['monolog']->info(
            sprintf(
                "Received request to process XML feed '%s'.",
                $feedURI
            )
        );
        $self = $this;

        $this->streamedResponse->setCallback(
            function () use ($self, $feedURI) {
                if (!$self->isValidFeedURI($feedURI)) {
                    $this->app['monolog']->error(
                        sprintf(
                            "Product XML feed [%s] is not a valid URL",
                            $feedURI
                        )
                    );
                    echo EventStream::createError([
                        sprintf(
                            'Product XML feed [%s] is not a valid URL',
                            $feedURI
                        )
                    ]);
                    flush();
                } else {
                    while (!$self->productReader->eof()) {
                        try {
                            $productCollection = $self->productReader->stream($feedURI);
                            if ($productCollection->count()) {
                                echo EventStream::create($productCollection);
                                flush();
                            }
                        } catch (OpenFeedURIFailedException $exp) {
                            $this->app['monolog']->error(
                                sprintf(
                                    "Problem occurred while opening feed [%s]. Error message: [%s]",
                                    $feedURI,
                                    $exp->getMessage()
                                )
                            );
                            echo EventStream::createError([
                                sprintf(
                                    'Failed to open product XML feed [%s]',
                                    $feedURI
                                )
                            ]);
                            flush();
                            break;
                        }
                    };
                }

                echo EventStream::createEOF();
                flush();
            }
        );
        $this->streamedResponse->headers->set('Content-Type', 'text/event-stream');
        $this->streamedResponse->headers->set('Cache-Control', 'no-cache');
        $this->streamedResponse->headers->set('X-Accel-Buffering', 'no');
        return $this->streamedResponse;
    }

    protected function isValidFeedURI(string $feedURI) : bool
    {
        return filter_var($feedURI, FILTER_VALIDATE_URL);
    }
}
