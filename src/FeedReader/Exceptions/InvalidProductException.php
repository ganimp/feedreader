<?php
namespace FeedReader\Exceptions;

use Doctrine\Instantiator\Exception\UnexpectedValueException;
use Exception;

class InvalidProductException extends UnexpectedValueException
{
    /**
     * Invalid product xml string
     *
     * @var string
     */
    private $productXML;

    /**
     * InvalidProductException constructor.
     *
     * @param string         $message
     * @param int            $code
     * @param Exception|null $previous
     * @param string         $productXML
     */
    public function __construct(
        string $message = 'Invalid product',
        int $code = 0,
        Exception $previous = null,
        string $productXML = ''
    ) {
        $this->productXML = $productXML;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Returns the invalid product xml
     *
     * @return mixed
     */
    public function getProductXML()
    {
        return $this->productXML;
    }
}
