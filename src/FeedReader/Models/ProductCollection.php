<?php
namespace FeedReader\Models;

use Countable;
use JsonSerializable;

class ProductCollection implements JsonSerializable, Countable
{
    /**
     * Collection of products
     *
     * @var Product[]
     */
    private $products = [];

    /**
     * Adds a product to the collection of products
     *
     * @param Product $product - product model
     *
     * @return ProductCollection
     */
    public function add(Product $product): ProductCollection
    {
        $this->products[] = $product;
        return $this;
    }

    /**
     * Allows to use the object in count() function
     *
     * @return int
     */
    public function count() : int
    {
        return count($this->products);
    }

    /**
     * Serializes the product collection to an array that can be serialized
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->products;
    }
}
