<?php
namespace FeedReader\Models;

use FeedReader\Exceptions\InvalidProductException;
use JsonSerializable;
use SimpleXMLElement;

class Product implements JsonSerializable
{
    /**
     * ID of the product
     *
     * @var string
     */
    private $productID;

    /**
     * Name of the product
     *
     * @var string
     */
    private $name;

    /**
     * Price of the product
     *
     * @var string
     */
    private $price;

    /**
     * Currency code
     *
     * @var string
     */
    private $currency;

    /**
     * URL of the product
     *
     * @var string
     */
    private $productURL;

    /**
     * URL of the product pics
     *
     * @var string
     */
    private $imageURL;

    /**
     * Description about the product
     *
     * @var string
     */
    private $description;

    /**
     * Categories of the product
     *
     * @var array
     */
    private $categories;

    /**
     * Array of required product XML elements
     *
     * @var array
     */
    private static $requiredElementsAndAttributes = [
        'productID',
        'name',
        'price',
        'currency',
        'imageURL',
        'productURL',
        'description',
        'categories'
    ];

    /**
     * Product constructor.
     *
     * @param string $productID   - ID of the product
     * @param string $name        - name of the product
     * @param string $price       - price of the product
     * @param string $currency    - currency code
     * @param string $productURL  - URL of the product
     * @param string $imageURL    - URL of product pics
     * @param string $description - Description about the product
     * @param array  $categories  - Categories of the product
     */
    public function __construct(
        string $productID,
        string $name,
        string $price,
        string $currency,
        string $productURL,
        string $imageURL,
        string $description,
        array $categories
    ) {
        $this->productID = $productID;
        $this->name = $name;
        $this->price = $price;
        $this->currency = $currency;
        $this->productURL = $productURL;
        $this->imageURL = $imageURL;
        $this->description = $description;
        $this->categories = $categories;
    }

    /**
     * Creates a product model from simple xml product element
     *
     * @param SimpleXMLElement $productXML - simple xml product element
     *
     * @return Product
     */
    public static function create(SimpleXMLElement $productXML) : Product
    {
        $properties = [];
        foreach ($productXML->children() as $child) {
            /** @var SimpleXmlElement $child */
            switch ($child->getName()) {
                case 'productID':
                    $properties['productID'] = (string) $child;
                    break;
                case 'name':
                    $properties['name'] = (string) $child;
                    break;
                case 'price':
                    $properties['price'] = (string) $child;
                    $priceAttributes = $child->attributes();
                    $properties['currency'] = (string) $priceAttributes['currency'];
                    break;
                case 'productURL':
                    $properties['productURL'] = (string) $child;
                    break;
                case 'imageURL':
                    $properties['imageURL'] = (string) $child;
                    break;
                case 'description':
                    $properties['description'] = (string) $child;
                    break;
                case 'categories':
                    $properties['categories'] = [];
                    foreach ($child->children() as $category) {
                        $properties['categories'][] = (string) $category;
                    }
                    break;
            }
        }
        $missingElements = array_diff(
            static::$requiredElementsAndAttributes,
            array_keys($properties)
        );
        if (count($missingElements) > 0) {
            throw new InvalidProductException(
                sprintf(
                    'Product element missing required elements and attributes [%s]. Product XML element received: [%s]',
                    implode(', ', $missingElements),
                    $productXML->asXML()
                ),
                1001,
                null,
                $productXML->asXML()
            );
        }

        return new self(
            $properties['productID'],
            $properties['name'],
            $properties['price'],
            $properties['currency'],
            $properties['productURL'],
            $properties['imageURL'],
            $properties['description'],
            $properties['categories']
        );
    }

    /**
     * Serializes the product model to an array that can be serialized
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'productID' => $this->productID,
            'name' => $this->name,
            'price' => $this->price,
            'currency' => $this->currency,
            'productURL' => $this->productURL,
            'imageURL' => $this->imageURL,
            'description' => $this->description,
            'categories' => $this->categories,
        ];
    }
}
