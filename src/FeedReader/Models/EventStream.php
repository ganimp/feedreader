<?php
/**
 * EventStream File
 *
 * PHP version 7.1
 *
 * @category Class
 * @package  FeedReader
 * @author   Gani <ganicvns@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://github.com/php
 */
namespace FeedReader\Models;

use JsonSerializable;

/**
 * EventStream Class
 *
 * @category Class
 * @package  FeedReader
 * @author   Gani <ganicvns@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://github.com/php
 */
class EventStream
{
    const STREAM_PREFIX = 'data: ';

    const STREAM_SUFFIX = " \n\n";

    const STREAM_EOF = ['eof' => true];

    /**
     * Creates a event stream string for the specified data
     *
     * @param JsonSerializable $streamData - data which should be serialized to JSON
     *
     * @return string
     */
    public static function create(JsonSerializable $streamData) : string
    {
        return sprintf(
            "%s%s%s",
            static::STREAM_PREFIX,
            json_encode($streamData),
            static::STREAM_SUFFIX
        );
    }

    /**
     * Creates a event stream for end of feed
     *
     * @return string
     */
    public static function createEOF() : string
    {
        return sprintf(
            "%s%s%s",
            static::STREAM_PREFIX,
            json_encode(static::STREAM_EOF),
            static::STREAM_SUFFIX
        );
    }

    /**
     * Creates a event stream for end of feed
     *
     * @param array $errors - array of error messages
     *
     * @return string
     */
    public static function createError(array $errors) : string
    {
        return sprintf(
            "%s%s%s",
            static::STREAM_PREFIX,
            json_encode(['error' => true, 'errors' => $errors]),
            static::STREAM_SUFFIX
        );
    }
}
