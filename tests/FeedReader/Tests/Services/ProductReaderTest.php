<?php
namespace FeedReader\Tests\Services;

use DOMDocument;
use FeedReader\Exceptions\OpenFeedURIFailedException;
use FeedReader\Models\EndOfFeed;
use FeedReader\Models\Product;
use FeedReader\Models\ProductCollection;
use FeedReader\Services\ProductReader;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Silex\Application;
use XMLReader;

class ProductReaderTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function setUp()
    {
        $this->app = new Application();
        $this->monolog = Mockery::mock(Logger::class);
        $this->app['monolog'] = $this->monolog;
        $this->xmlReader = Mockery::mock(XMLReader::class);
        $this->dom = new DOMDocument();
    }

    public function testConstructor()
    {
        $productReader = new ProductReader(
            $this->app,
            $this->xmlReader,
            $this->dom
        );
        $this->assertInstanceOf(ProductReader::class, $productReader);
    }

    public function testItThrowsExceptionWhenItFailedToOpenFeedURI()
    {
        $numOfProductsToRead = 2;
        $feedURI = 'http://localhost/assets/data/test-productfeed.xml';

        $this->monolog->shouldNotReceive('warning');

        $this->xmlReader->shouldReceive('open')
            ->once()
            ->with($feedURI)
            ->andReturn(false);
        $productReader = new ProductReader(
            $this->app,
            $this->xmlReader,
            $this->dom,
            $numOfProductsToRead
        );

        $this->expectException(OpenFeedURIFailedException::class);
        $productReader->stream($feedURI);
    }

    public function testItStreamsChunkOfFeedAndLogsInvalidProductXML()
    {
        $numOfProductsToRead = 2;
        $feedURI = 'http://example.com/productfeed.xml';

        $productXML1 = <<<EOD
<product>
<productID>productID1</productID>
<name>name1</name>
<price currency="EUR">100.00</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?productID=1</productURL>
<imageURL>http://www.centralpoint.nl/tracker/productID-1.jpg</imageURL>
<description><![CDATA[description1]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $productXML2 = <<<EOD
<product>
<productID>productID2</productID>
<name>name2</name>
<price currency="EUR">200.00</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?productID=2</productURL>
<imageURL>http://www.centralpoint.nl/tracker/productID-2.jpg</imageURL>
<description><![CDATA[description2]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $dom = new DOMDocument();
        $dom->loadXML($productXML1);
        $node1 = $dom->getElementsByTagName('product')->item(0);
        $dom->loadXML($productXML2);
        $node2 = $dom->getElementsByTagName('product')->item(0);

        $this->monolog->shouldNotReceive('warning');

        $this->xmlReader->shouldReceive('open')
            ->once()
            ->with($feedURI)
            ->andReturn(true);
        $this->xmlReader->shouldReceive('read')
            ->twice()
            ->andReturn(true);
        $this->xmlReader->shouldReceive('expand')
            ->twice()
            ->andReturn($node1, $node2);
        $this->xmlReader->shouldReceive('next')
            ->twice()
            ->andReturn(true);
        $productReader = Mockery::mock(
            ProductReader::class . '[getNameOfCurrentNode]',
            [
                $this->app,
                $this->xmlReader,
                $this->dom,
                $numOfProductsToRead
            ]
        )->shouldAllowMockingProtectedMethods();

        $productReader->shouldReceive('getNameOfCurrentNode')
            ->twice()
            ->andReturn('product');

        $expected = new ProductCollection();
        $expected->add(
            new Product(
                'productID1',
                'name1',
                '100.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=1',
                'http://www.centralpoint.nl/tracker/productID-1.jpg',
                'description1',
                ['computers']
            )
        );
        $expected->add(
            new Product(
                'productID2',
                'name2',
                '200.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=2',
                'http://www.centralpoint.nl/tracker/productID-2.jpg',
                'description2',
                ['computers']
            )
        );

        $actual = $productReader->stream($feedURI);
        $this->assertEquals($expected, $actual);
        $this->assertCount($numOfProductsToRead, $actual);
        $this->assertFalse($productReader->eof());
    }

    public function testItStreamsFeedUntilEOF()
    {
        $numOfProductsToRead = 5;
        $feedURI = 'http://example.com/productfeed.xml';

        $productXML1 = <<<EOD
<product>
<productID>productID1</productID>
<name>name1</name>
<price currency="EUR">100.00</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?productID=1</productURL>
<imageURL>http://www.centralpoint.nl/tracker/productID-1.jpg</imageURL>
<description><![CDATA[description1]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $productXML2 = <<<EOD
<product>
<name>name2</name>
<price currency="EUR">200.00</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?productID=2</productURL>
<imageURL>http://www.centralpoint.nl/tracker/productID-2.jpg</imageURL>
<description><![CDATA[description2]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $dom = new DOMDocument();
        $dom->loadXML($productXML1);
        $node1 = $dom->getElementsByTagName('product')->item(0);
        $dom->loadXML($productXML2);
        $node2 = $dom->getElementsByTagName('product')->item(0);

        $this->monolog->shouldReceive('warning')->once();

        $this->xmlReader->shouldReceive('open')
            ->once()
            ->with($feedURI)
            ->andReturn(true);
        $this->xmlReader->shouldReceive('read')
            ->times(3)
            ->andReturn(true, true, false);
        $this->xmlReader->shouldReceive('expand')
            ->twice()
            ->andReturn($node1, $node2);
        $this->xmlReader->shouldReceive('next')
            ->twice()
            ->andReturn(true);
        $this->xmlReader->shouldReceive('close')
            ->once()
            ->andReturn(true);
        $productReader = Mockery::mock(
            ProductReader::class . '[getNameOfCurrentNode]',
            [
                $this->app,
                $this->xmlReader,
                $this->dom,
                $numOfProductsToRead
            ]
        )->shouldAllowMockingProtectedMethods();

        $productReader->shouldReceive('getNameOfCurrentNode')
            ->twice()
            ->andReturn('product');

        $expected = new ProductCollection();
        $expected->add(
            new Product(
                'productID1',
                'name1',
                '100.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=1',
                'http://www.centralpoint.nl/tracker/productID-1.jpg',
                'description1',
                ['computers']
            )
        );

        $actual = $productReader->stream($feedURI);
        $this->assertEquals($expected, $actual);
        $this->assertLessThanOrEqual($numOfProductsToRead, count($actual));
        $this->assertTrue($productReader->eof());
    }
}
