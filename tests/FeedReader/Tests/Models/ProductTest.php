<?php
namespace FeedReader\Tests\Models;

use FeedReader\Exceptions\InvalidProductException;
use FeedReader\Models\Product;
use JsonSerializable;
use PHPUnit\Framework\TestCase;
use SimpleXMLElement;

class ProductTest extends TestCase
{
    public function testItCanCreateProduct()
    {
        $productXML = <<<EOD
<product>
<productID>2-power_lcdcable3</productID>
<name>2-Power LCD-LED Screen Converter Cable - LCDCABLE3 (LCDCABLE3)</name>
<price currency="EUR">19.36</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?tt=534_251713_1_&amp;r=https%3A%2F%2Fwww.centralpoint.nl%2Fnotebooks-laptops%2F2-power%2Flcd-led-screen-converter-cable-lcdcable3-art-lcdcable3-num-6030989%2F</productURL>
<imageURL>http://www02.cp-static.com/images/pna_fo.jpg</imageURL>
<description><![CDATA[LCD-LED Screen Converter Cable]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $element = new SimpleXMLElement($productXML);
        $product = Product::create($element);
        $this->assertInstanceOf(Product::class, $product);
    }

    public function testItThrowsExceptionWhenElementMissingProperties()
    {
        $productXML = <<<EOD
<product>
<name>2-Power LCD-LED Screen Converter Cable - LCDCABLE3 (LCDCABLE3)</name>
<price currency="EUR">19.36</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?tt=534_251713_1_&amp;r=https%3A%2F%2Fwww.centralpoint.nl%2Fnotebooks-laptops%2F2-power%2Flcd-led-screen-converter-cable-lcdcable3-art-lcdcable3-num-6030989%2F</productURL>
<imageURL>http://www02.cp-static.com/images/pna_fo.jpg</imageURL>
<description><![CDATA[LCD-LED Screen Converter Cable]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $element = new SimpleXMLElement($productXML);
        $this->expectException(InvalidProductException::class);
        Product::create($element);
    }

    public function testItImplementsJsonSerializable()
    {
        $productID = '2-power_lcdcable3';
        $name = '2-Power LCD-LED Screen Converter Cable - LCDCABLE3 (LCDCABLE3)';
        $description = 'LCD-LED Screen Converter Cable';
        $price = '19.36';
        $currency = 'EUR';
        $productURL = 'http://www.centralpoint.nl/tracker/index.php?tt=534_251713_1_&amp;r=https%3A%2F%2Fwww.centralpoint.nl%2Fnotebooks-laptops%2F2-power%2Flcd-led-screen-converter-cable-lcdcable3-art-lcdcable3-num-6030989%2F';
        $imageURL = 'http://www02.cp-static.com/images/pna_fo.jpg';
        $productXML = <<<EOD
<product>
<productID>$productID</productID>
<name>$name</name>
<price currency="$currency">$price</price>
<productURL>$productURL</productURL>
<imageURL>$imageURL</imageURL>
<description><![CDATA[$description]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $expected = [
            'productID' => $productID,
            'name' => $name,
            'price' => $price,
            'currency' => $currency,
            'productURL' => html_entity_decode($productURL),
            'imageURL' => html_entity_decode($imageURL),
            'description' => $description,
            'categories' => ['computers']
        ];
        $element = new SimpleXMLElement($productXML);
        $product = Product::create($element);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertInstanceOf(JsonSerializable::class, $product);
        $this->assertEquals(
            json_encode($expected, true),
            json_encode($product, true)
        );
    }
}
