<?php
namespace FeedReader\Tests\Models;

use Countable;
use FeedReader\Models\Product;
use FeedReader\Models\ProductCollection;
use JsonSerializable;
use PHPUnit\Framework\TestCase;
use SimpleXMLElement;

class ProductCollectionTest extends TestCase
{
    public function testItImplementsJsonSerializable()
    {
        $productXML = <<<EOD
<product>
<productID>productID1</productID>
<name>name1</name>
<price currency="EUR">100.00</price>
<productURL>http://www.centralpoint.nl/tracker/index.php?productID=1</productURL>
<imageURL>http://www.centralpoint.nl/tracker/productID-1.jpg</imageURL>
<description><![CDATA[description1]]></description>
<categories>
<category path="computers">computers</category>
</categories>
</product>
EOD;
        $expected = [
            [
                'productID' => 'productID1',
                'name' => 'name1',
                'price' => '100.00',
                'currency' => 'EUR',
                'productURL' => 'http://www.centralpoint.nl/tracker/index.php?productID=1',
                'imageURL' => 'http://www.centralpoint.nl/tracker/productID-1.jpg',
                'description' => 'description1',
                'categories' =>  ['computers']
            ]
        ];
        $element = new SimpleXMLElement($productXML);
        $productCollection = new ProductCollection();
        $productCollection->add(Product::create($element));
        $this->assertInstanceOf(ProductCollection::class, $productCollection);
        $this->assertInstanceOf(JsonSerializable::class, $productCollection);
        $this->assertInstanceOf(Countable::class, $productCollection);
        $this->assertCount(1, $productCollection);
        $this->assertEquals(
            json_encode($expected, true),
            json_encode($productCollection, true)
        );
    }
}
