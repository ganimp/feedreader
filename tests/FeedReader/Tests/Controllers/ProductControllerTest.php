<?php
namespace FeedReader\Tests\Controllers;

use FeedReader\Controllers\ProductController;
use FeedReader\Exceptions\OpenFeedURIFailedException;
use FeedReader\Models\EventStream;
use FeedReader\Models\Product;
use FeedReader\Models\ProductCollection;
use FeedReader\Services\ProductReader;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductControllerTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var  Application  */
    private $app;

    /** @var  MockInterface  */
    private $request;

    /** @var  MockInterface  */
    private $requestStack;

    /** @var  MockInterface  */
    private $productReader;

    /** @var  StreamedResponse  */
    private $streamedResponse;

    /** @var  ProductController  */
    private $productController;

    public function setUp()
    {
        $this->app = new Application();
        $this->monolog = Mockery::mock(Logger::class);
        $this->request = Mockery::mock(Request::class);
        $this->requestStack = Mockery::mock(RequestStack::class);
        $this->productReader = Mockery::mock(ProductReader::class);
        $this->streamedResponse = new StreamedResponse();
        $this->app['monolog'] = $this->monolog;
        $this->app['request_stack'] = $this->requestStack;
        $this->app['product.reader'] = $this->productReader;
        $this->productController = new ProductController(
            $this->app,
            $this->streamedResponse
        );
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(ProductController::class, $this->productController);
    }

    public function testItReturnsErrorResponseOnInvalidFeedURI()
    {
        $feedURI = 'invalid feed uri';

        $this->monolog->shouldReceive('info')->once();
        $this->monolog->shouldReceive('error')->once();

        $this->requestStack->shouldReceive('getCurrentRequest')
            ->once()
            ->andReturn($this->request);

        $this->request->shouldReceive('get')
            ->once()
            ->with('feedURI')
            ->andReturn($feedURI);

        $this->productReader->shouldNotReceive('stream');
        $response = $this->productController->getProductsJsonAction();
        $response->send();
        $expectedResponse = sprintf(
            "%s%s",
            EventStream::createError([
                sprintf(
                    'Product XML feed [%s] is not a valid URL',
                    $feedURI
                )
            ]),
            EventStream::createEOF()
        );
        $this->expectOutputString($expectedResponse);
    }

    public function testItReturnsErrorResponseWhenProductReaderFailedToOpenFeedURI()
    {
        $feedURI = 'http://localhost/assets/data/test-productfeed.xml';

        $this->monolog->shouldReceive('info')->once();
        $this->monolog->shouldReceive('error')->once();

        $this->requestStack->shouldReceive('getCurrentRequest')
            ->once()
            ->andReturn($this->request);

        $this->request->shouldReceive('get')
            ->once()
            ->with('feedURI')
            ->andReturn($feedURI);

        $this->productReader->shouldReceive('stream')
            ->once()
            ->with($feedURI)
            ->andThrow(new OpenFeedURIFailedException());
        $this->productReader->shouldReceive('eof')
            ->once()
            ->andReturn(false);
        $response = $this->productController->getProductsJsonAction();
        $response->send();
        $expectedResponse = sprintf(
            "%s%s",
            EventStream::createError([
                sprintf(
                    'Failed to open product XML feed [%s]',
                    $feedURI
                )
            ]),
            EventStream::createEOF()
        );
        $this->expectOutputString($expectedResponse);
    }

    public function testItStreamsProductsFeed()
    {
        $feedURI = 'http://localhost/assets/data/test-productfeed.xml';

        $this->monolog->shouldReceive('info')->once();

        $this->requestStack->shouldReceive('getCurrentRequest')
            ->once()
            ->andReturn($this->request);

        $this->request->shouldReceive('get')
            ->once()
            ->with('feedURI')
            ->andReturn($feedURI);

        $productCollection = new ProductCollection();
        $productCollection->add(
            new Product(
                'productID1',
                'name1',
                '100.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=1',
                'http://www.centralpoint.nl/tracker/productID-1.jpg',
                'description1',
                ['computers']
            )
        );
        $productCollection->add(
            new Product(
                'productID2',
                'name2',
                '200.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=2',
                'http://www.centralpoint.nl/tracker/productID-2.jpg',
                'description2',
                ['computers']
            )
        );
        $productCollection->add(
            new Product(
                'productID3',
                'name3',
                '300.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=3',
                'http://www.centralpoint.nl/tracker/productID-3.jpg',
                'description3',
                ['computers']
            )
        );
        $productCollection->add(
            new Product(
                'productID4',
                'name4',
                '400.00',
                'EUR',
                'http://www.centralpoint.nl/tracker/index.php?productID=4',
                'http://www.centralpoint.nl/tracker/productID-4.jpg',
                'description4',
                ['computers']
            )
        );

        $this->productReader->shouldReceive('stream')
            ->once()
            ->with($feedURI)
            ->andReturn($productCollection);

        $this->productReader->shouldReceive('eof')
            ->once()
            ->andReturn(false);
        $this->productReader->shouldReceive('eof')
            ->once()
            ->andReturn(true);

        $response = $this->productController->getProductsJsonAction();
        $response->send();
        $expectedResponse = sprintf(
            "%s%s",
            EventStream::create($productCollection),
            EventStream::createEOF()
        );
        $this->expectOutputString($expectedResponse);
    }
}
