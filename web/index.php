<?php
require_once __DIR__.'/../bootstrap.php';

use FeedReader\Controllers\ProductController;
use FeedReader\Services\ProductReader;
use Symfony\Component\HttpFoundation\StreamedResponse;

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', []);
});

$app['product.reader'] = function () use ($app) {
    return new ProductReader(
        $app,
        new XMLReader(),
        new DOMDocument()
    );
};

$app['products.controller'] = function () use ($app) {
    return new ProductController(
        $app,
        new StreamedResponse()
    );
};

$app->get('/products.json', 'products.controller:getProductsJsonAction');

$app->run();
