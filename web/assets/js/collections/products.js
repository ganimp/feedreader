var app = app || {};

(function () {
    'use strict';

    // Product Collection
    // ---------------
    var Products = Backbone.Collection.extend({
        model: app.Product
    });

    // Create our global collection of **Products**.
    app.products = new Products();
})();
