var app = app || {};

(function () {
    'use strict';

    // Product Model
    // ----------
    app.Product = Backbone.Model.extend({
        // default attributes for the Product
        defaults: {
            productID: '',
            name: '',
            price: '',
            currency: '',
            productURL: '',
            imageURL: '',
            description: ''
        }
    });
})();
