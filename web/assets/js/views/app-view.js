var app = app || {};

(function ($) {
    'use strict';

    // The Application
    // ---------------
    app.AppView = Backbone.View.extend({
        el: '.feedreaderapp',
        events: {
            'keypress .input-feed-uri': 'readOnEnter'
        },
        initialize: function () {
            this.$input = this.$('.input-feed-uri');
            this.$footer = this.$('.footer');
            this.$main = this.$('.main');
            this.$list = $('.product-list');

            this.listenTo(app.products, 'add', this.addOne);
            this.listenTo(app.products, 'reset', this.addAll);
        },
        render: function () {
            if (app.products.length) {
                this.$main.show();
                this.$footer.show();
            } else {
                this.$main.hide();
                this.$footer.hide();
            }
        },

        // Add a single product item to the list by creating a view for it, and
        // appending its element to the `<ul>`.
        addOne: function (product) {
            var view = new app.ProductView({ model: product });
            this.$list.append(view.render().el);
        },
        // Add all items in the **Products** collection at once.
        addAll: function () {
            this.$list.html('');
            app.products.each(this.addOne, this);
        },

        // when return key is pressed after entering product feed URI in main input field, read products from specified feed.
        readOnEnter: function (e) {
            if (e.which === ENTER_KEY && this.$input.val().trim()) {
                app.products.reset();
                console.log(app.products);
                $('.has-error').removeClass('has-error');
                $('.alert').html('').hide();
                e.stopPropagation();
                e.preventDefault();
                var feedURI = $('.input-feed-uri').val();

                var evtSource = new EventSource('/products.json?feedURI=' + encodeURIComponent(feedURI));

                evtSource.onopen = function () {
                    console.log("Connection to server opened.");
                };

                evtSource.onerror = function () {
                    console.log("EventSource failed.");
                };

                evtSource.onmessage = function (e) {
                    /**
                     * @param {{data:{eof: bool}}} e
                     */
                    var data = JSON.parse(e.data);
                    if (data.eof) {
                        evtSource.close();
                    } else if (data.error) {
                        console.log(data.errors);
                        evtSource.close();
                        $('.has-error').removeClass('has-error');
                        $('.alert').html(_.values(data.errors).join('<br>')).show();

                    } else  {
                        _.map(data, function (product) {
                            app.products.add(new app.Product({
                                productID: product.productID,
                                name: product.name,
                                price: product.price,
                                currency: product.currency,
                                description: product.description,
                                productURL: product.productURL,
                                imageURL: product.imageURL,
                                categories: product.categories
                            }));
                        });
                    }
                };
                this.$input.val('');
            }
        }
    });
})(jQuery);
