var app = app || {};

(function ($) {
    'use strict';

    // Product Item View
    // --------------
    app.ProductView = Backbone.View.extend({
        tagName:  'li',
        template: _.template($('#item-template').html()),
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.$input = this.$('.edit');
            return this;
        },
        clear: function () {
            this.model.destroy();
        }
    });
})(jQuery);
