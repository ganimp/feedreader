# Developer Assessment

Feed processing function to handle very large feeds of a fixed format.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

You will need

* PHP 5.3.2 or above
* Docker
* Docker Compose
* GNU make utility
* Bower

## Setup

This repo includes a set of **GNU Make** utility commands that will help you to build and manage the application quick and fast.

```
make start-app
```

### Checking docker container status

```
make state
```

### Stopping docker container

```
make down
```

## Manual testing

Visit http://127.0.0.1:8000. When the web page is loaded, enter the product feed URL and press **Enter**.

![Alt text](/screenshot.png?raw=true "Screenshot")

## Automated testing

Frameworks used,

* [PHPUnit](https://phpunit.de/) - Unit testing framework for PHP.
* [Behat](http://behat.org/en/latest/) - A test framework for behavior-driven development.
* [Mockery](http://docs.mockery.io/en/latest/)   - PHP mock object framework for use in unit testing with PHPUnit.
* [PHP_CodeSniffer](https://www.squizlabs.com/php-codesniffer) - Detects violations of a defined set of coding standards.

## Running the tests

### Unit test

```
make test
```
### Behat test


Running behat tests (human-readable stories that describe the behavior of the application). 

```
make behat
```

### And coding style tests

```
make code-sniffer-cli
```

## Built With

* [Silex](https://silex.sensiolabs.org/) - PHP micro framework built on the shoulders of Symfony used for backend 
* [Nginx](https://nginx.org/en/) - Open-source, high-performance HTTP server.
* [Backbone.js](http://backbonejs.org/) - Javascript MV* framework used for front end.
* [Twig](https://twig.sensiolabs.org/) - Template engine for PHP.
* [Bootstrap](http://getbootstrap.com/css/) - A CSS framework.
* [Underscore](http://underscorejs.org/) - JavaScript library that provides useful functional programming helpers without extending any built-in objects.


## Authors

* **Gani** 

## License

This project is licensed under the MIT License

## Acknowledgments

* [Backbon.js TodoMVC Example](https://github.com/tastejs/todomvc/tree/master/examples/backbone) -  Todo example app built with Backbone.js,
* [Eventsource polyfill](https://github.com/Yaffle/EventSource) - A browser fallback polyfill for EventSource Web API.
